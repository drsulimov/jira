package com.cgi.jira.endpoints;

import com.cgi.jira.services.TicketService;
import com.cgi.ws.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.time.Instant;
import java.util.GregorianCalendar;

@Endpoint
public class TicketEndpoint {
    private static final String NAMESPACE_URI = "http://cgi.com/ws";
    private TicketService ticketService;

    @Autowired
    public TicketEndpoint(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getTicketByIdRequest")
    @ResponsePayload
    public GetTicketByIdResponse getTicketById(@RequestPayload GetTicketByIdRequest request) {
        GetTicketByIdResponse getTicketByIdResponse = new GetTicketByIdResponse();
        getTicketByIdResponse.setTicketById(ticketService.getTicketById(request.getId()));
        return getTicketByIdResponse;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getTicketByNameRequest")
    @ResponsePayload
    public GetTicketByNameResponse getTicketByName(@RequestPayload GetTicketByNameRequest request) {
        GetTicketByNameResponse getTicketByNameResponse = new GetTicketByNameResponse();
        getTicketByNameResponse.setTicketByName(ticketService.getTicketByName(request.getName()));
        return getTicketByNameResponse;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "saveTicketRequest")
    @ResponsePayload
    public SaveTicketResponse saveTicket(@RequestPayload SaveTicketRequest request) {
        SaveTicketResponse saveTicketResponse = new SaveTicketResponse();
        GregorianCalendar ticketCreationTime = new GregorianCalendar();
        ticketCreationTime.setTimeInMillis(Instant.now().toEpochMilli());
        try {
            request.getTicket().setCreationDatetime(DatatypeFactory.newInstance().newXMLGregorianCalendar(ticketCreationTime));
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
        saveTicketResponse.setSaveTicket(ticketService.saveTicket(request.getTicket()));
        return saveTicketResponse;
    }
}
