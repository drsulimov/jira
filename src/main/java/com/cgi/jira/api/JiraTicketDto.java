package com.cgi.jira.api;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class JiraTicketDto implements Serializable {
    private Long id;
    private String name;
    private String email;
    private Long idPersonCreator;
    private Long idPersonAssigned;
    private Timestamp creationDatetime;
    private Timestamp ticketCloseDatetime;
}
