package com.cgi.jira.services;

import com.cgi.ws.SaveTicket;
import com.cgi.ws.TicketById;
import com.cgi.ws.TicketByName;

public interface TicketService {
    TicketById getTicketById(Long id);

    TicketByName getTicketByName(String name);

    int saveTicket(SaveTicket ticket);
}
