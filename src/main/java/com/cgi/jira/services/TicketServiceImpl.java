package com.cgi.jira.services;

import com.cgi.jira.api.JiraTicketDto;
import com.cgi.jira.entities.Ticket;
import com.cgi.jira.mappers.BeanMapping;
import com.cgi.jira.repositories.TicketRepository;
import com.cgi.ws.SaveTicket;
import com.cgi.ws.TicketById;
import com.cgi.ws.TicketByName;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.util.GregorianCalendar;

@Service
public class TicketServiceImpl implements TicketService {
    private TicketRepository ticketRepository;
    private RabbitTemplate rabbitTemplate;
    private BeanMapping beanMapping;

    @Autowired
    public TicketServiceImpl(TicketRepository ticketRepository, RabbitTemplate rabbitTemplate, BeanMapping beanMapping) {
        this.ticketRepository = ticketRepository;
        this.rabbitTemplate = rabbitTemplate;
        this.beanMapping = beanMapping;
    }

    private TicketById mapTicketToTicketById(Ticket ticket) {
        TicketById ticketById = new TicketById();
        ticketById.setId(ticket.getId());
        ticketById.setName(ticket.getName());
        ticketById.setEmail(ticket.getEmail());
        ticketById.setIdPersonCreator(ticket.getIdPersonCreator());
        ticketById.setIdPersonAssigned(ticket.getIdPersonAssigned());

        GregorianCalendar ticketCreationTime = new GregorianCalendar();
        ticketCreationTime.setTimeInMillis(ticket.getCreationDatetime().getTime());

        GregorianCalendar ticketCloseTime = new GregorianCalendar();
        ticketCloseTime.setTimeInMillis(ticket.getTicketCloseDatetime().getTime());

        try {
            ticketById.setCreationDatetime(DatatypeFactory.newInstance().newXMLGregorianCalendar(ticketCreationTime));
            ticketById.setTicketCloseDatetime(DatatypeFactory.newInstance().newXMLGregorianCalendar(ticketCloseTime));
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }

        return ticketById;
    }

    private TicketByName mapTicketToTicketByName(Ticket ticket) {
        TicketByName ticketByName = new TicketByName();
        ticketByName.setId(ticket.getId());
        ticketByName.setName(ticket.getName());
        ticketByName.setEmail(ticket.getEmail());
        ticketByName.setIdPersonCreator(ticket.getIdPersonCreator());
        ticketByName.setIdPersonAssigned(ticket.getIdPersonAssigned());

        GregorianCalendar ticketCreationTime = new GregorianCalendar();
        ticketCreationTime.setTimeInMillis(ticket.getCreationDatetime().getTime());

        GregorianCalendar ticketCloseTime = new GregorianCalendar();
        ticketCloseTime.setTimeInMillis(ticket.getTicketCloseDatetime().getTime());

        try {
            ticketByName.setCreationDatetime(DatatypeFactory.newInstance().newXMLGregorianCalendar(ticketCreationTime));
            ticketByName.setTicketCloseDatetime(DatatypeFactory.newInstance().newXMLGregorianCalendar(ticketCloseTime));
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }

        return ticketByName;
    }

    @Override
    public TicketById getTicketById(Long id) {
        Ticket ticket = ticketRepository.getTicketById(id);
        return mapTicketToTicketById(ticket);
    }

    @Override
    public TicketByName getTicketByName(String name) {
        Ticket ticket = ticketRepository.getTicketByName(name);
        return mapTicketToTicketByName(ticket);
    }

    @Override
    public int saveTicket(SaveTicket ticket) {
        JiraTicketDto jiraTicket = beanMapping.mapTo(ticket, JiraTicketDto.class);
        rabbitTemplate.convertAndSend("jira_tickets", jiraTicket);
        return ticketRepository.saveTicket(ticket);
    }
}
