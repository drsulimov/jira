package com.cgi.jira.entities;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class Ticket implements Serializable {
    private Long id;
    private String name;
    private String email;
    private Long idPersonCreator;
    private Long idPersonAssigned;
    private Timestamp creationDatetime;
    private Timestamp ticketCloseDatetime;
}
