package com.cgi.jira.repositories;

import com.cgi.jira.entities.Ticket;
import com.cgi.jira.mappers.TicketMapper;
import com.cgi.ws.SaveTicket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.time.Instant;

@Repository
public class TicketRepository {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public TicketRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Ticket getTicketById(Long id) {
        return jdbcTemplate.queryForObject("SELECT * FROM ticket t WHERE t.id=?",
                new Object[]{id},
                new TicketMapper());
    }

    public Ticket getTicketByName(String name) {
        return jdbcTemplate.queryForObject("SELECT * FROM ticket t WHERE t.name=?",
                new Object[]{name},
                new TicketMapper());
    }

    public int saveTicket(SaveTicket ticket) {
        Timestamp creationTimeAsTimestamp = new Timestamp(Instant.now().toEpochMilli());
        return jdbcTemplate.update("INSERT INTO ticket VALUES (?, ?, ?, ?, ?, ?)",
                ticket.getId(),
                ticket.getName(),
                ticket.getEmail(),
                ticket.getIdPersonCreator(),
                ticket.getIdPersonAssigned(),
                creationTimeAsTimestamp);
    }
}
