package com.cgi.jira.mappers;

import com.cgi.jira.entities.Ticket;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TicketMapper implements RowMapper<Ticket> {
    @Override
    public Ticket mapRow(ResultSet resultSet, int i) throws SQLException {
        Ticket ticket = new Ticket();
        ticket.setId(resultSet.getLong("id"));
        ticket.setName(resultSet.getString("name"));
        ticket.setEmail(resultSet.getString("email"));
        ticket.setIdPersonCreator(resultSet.getLong("id_person_creator"));
        ticket.setIdPersonAssigned(resultSet.getLong("id_person_assigned"));
        ticket.setCreationDatetime(resultSet.getTimestamp("creation_datetime"));
        ticket.setTicketCloseDatetime(resultSet.getTimestamp("ticket_close_datetime"));
        return ticket;
    }
}
